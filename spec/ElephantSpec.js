describe('Elephant', function () {

    beforeEach(function () {
        this.elephant = new Elephant('Nelly', 'African', 'pink');
    });

    afterEach(function () {
        delete this.elephant;
    });

    describe('elephant', function () {

        it('is named Nelly', function () {
            expect(this.elephant.name).toBe('Nelly');
        });

        it('has origin African', function () {
            expect(this.elephant.origin).toBe('African');
        });

        it('is pink', function () {
            expect(this.elephant.colour).toBe('pink');
        });

    });

    describe('getColour', function () {
        it('returns the colour of the elephant', function () {
            expect(this.elephant.getColour()).toBe('pink');
        });
    });

    describe('rename to Noa', function () {

        beforeEach(function () {
            this.elephant.rename('Noa');
        });

        it('renames the Elephant to Noa', function () {
            expect(this.elephant.name).toBe('Noa');
        });

    });

    describe('origin', function () {

        beforeEach(function () {
            this.elephant.origin = 'Indian';
        });

        it('stays African', function () {
            expect(this.elephant.origin).toBe('African');
        });

    });

    describe('paint', function () {

        beforeEach(function () {
            this.elephant.paint('green');
        });

        it('changes the colour of the Elephant to green', function () {
            expect(this.elephant.colour).toBe('green');
        });

    });

    describe('hungry - empty stomach', function () {

        beforeEach(function () {
            spyOn(Elephant.prototype, 'stomach').and.returnValue([]);
        });

        it('is hungry', function () {
            expect(this.elephant.hungry()).toBe(true);
        });

    });

    describe('hungry - filled stomach', function () {

        beforeEach(function () {
            spyOn(Elephant.prototype, 'stomach').and.returnValue(['cheese', 'bread', 'lettuce', 'cheese', 'bread']);
        });

        it('is not hungry', function () {
            expect(this.elephant.hungry()).toBe(false);
        });

    });

    describe('stomach', function () {

        beforeEach(function () {
            this.elephant.stomachContents = [];
        });

        it('takes cheese', function () {
            this.elephant.stomach('cheese');
            expect(this.elephant.stomach()).toContain('cheese');
            expect(this.elephant.stomachContents.length).toEqual(1);
        });

        it('takes more than one piece of food', function () {
            this.elephant.stomach('lettuce', 'tomatoe');
            expect(this.elephant.stomach()[0]).toBe('lettuce');
            expect(this.elephant.stomach()[1]).toBe('tomatoe');
        });

        it('gets full - maximum five objects', function () {
            this.elephant.stomach('lettuce', 'tomatoe');
            this.elephant.stomach('lettuce', 'tomatoe');
            this.elephant.stomach('lettuce', 'tomatoe');
            expect(this.elephant.stomach().length).toEqual(5);
        });

        it('digests food from first position', function (done) {
            var that = this;
            setTimeout(function () {
                expect(that.elephant.stomachContents.length).toEqual(1);
                expect(that.elephant.stomach()).toContain('ham');
                expect(that.elephant.stomach()).not.toContain('cheese');
                done();
            }, 1000);
            this.elephant.stomach('cheese');
            this.elephant.stomach('ham');
        });

    });

    describe('feedElephant - hungry', function () {

        beforeEach(function () {
            spyOn(Elephant.prototype, 'hungry').and.returnValue(true);
            spyOn(Elephant.prototype, 'stomach').and.callFake(function () {
            });
        });

        it('eats when hungry', function () {
            expect(this.elephant.feedElephant('cheese')).toBe(true);
            expect(this.elephant.stomach).toHaveBeenCalledWith('cheese');
        });

    });

    describe('feedElephant - not hungry', function () {

        beforeEach(function () {
            spyOn(Elephant.prototype, 'hungry').and.returnValue(false);
            spyOn(Elephant.prototype, 'stomach').and.callFake(function () {
            });
        });

        it('doesn\'t eat when not hungry', function () {
            expect(this.elephant.feedElephant('cheese')).toBe(false);
            expect(this.elephant.stomach).not.toHaveBeenCalled();
        });


    });

});
