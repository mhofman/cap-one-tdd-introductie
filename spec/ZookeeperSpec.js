describe('Zookeeper', function () {

    var Zoo = {
        get: null,
        post: null,
        Elephant: [],
        Tiger: []
    };

    var InstructionStub = [
        {
            Elephant: {
                Feed: 'Cheese'
            }
        },
        {
            Tiger: {
                Feed: 'Bacon'
            }
        },
        {
            Snake: {
                Feed: 'Mouse'
            }
        }
    ];

    beforeEach(function () {
        this.zookeeper = new Zookeeper(Zoo, 'John', ['Elephant', 'Tiger'], 'Steve');
    });

    afterEach(function () {
        delete this.zookeeper;
    });


    describe('John', function () {

        it('is integrated with the Zoo', function () {
            expect(this.zookeeper.Zoo).toEqual(Zoo);
        });

        it('is named John', function () {
            expect(this.zookeeper.name).toBe('John');
        });

        it('is specialised in Elephant and Tiger', function () {
            expect(this.zookeeper.specialisations).toContain('Elephant');
            expect(this.zookeeper.specialisations).toContain('Tiger');
        });

        it('has a Boss named Steve', function () {
            expect(this.zookeeper.boss).toBe('Steve');
        });

    });

    describe('askBossForInstructions', function () {

        beforeEach(function () {
            spyOn(Zoo, 'get').and.returnValue(InstructionStub);
            this.zookeeper.askBossForInstructions();
        });

        it('asks Steve for instructions', function () {
            expect(Zoo.get).toHaveBeenCalledWith('Steve/instructions/John');
        });

        it('updates instructions', function () {
            expect(this.zookeeper.instructions).toEqual(InstructionStub);
        });

    });

    describe('reportToBoss', function () {
        beforeEach(function () {
            spyOn(Zoo, 'post').and.callFake(function () {
            });
            this.zookeeper.reportToBoss({});
        });

        afterEach(function () {
        });

        it('posts the report to the boss', function () {
            expect(Zoo.post).toHaveBeenCalledWith('Steve/reports/', {});
        });

    });

    describe('followInstruction ', function () {
        beforeEach(function () {
            this.elephant = jasmine.createSpyObj('elephant', ['feedElephant']);
            this.tiger = jasmine.createSpyObj('tiger', ['feedTiger']);


            Zoo.Elephant.push(this.elephant);
            Zoo.Tiger.push(this.tiger);

            this.zookeeper.followInstruction({
                Elephant: {
                    Feed: 'Cheese'
                }
            });

            this.zookeeper.followInstruction({
                Tiger: {
                    Feed: 'Bacon'
                }
            });

        });

        it('Knows how to feeds the elephants', function () {
            expect(this.elephant.feedElephant).toHaveBeenCalledWith('Cheese');
        });

        it('Knows how to feed the tigers', function () {
            expect(this.tiger.feedTiger).toHaveBeenCalledWith('Bacon');
        });

    });

    describe('followInstructions', function () {
        beforeEach(function () {
            this._instructions = this.zookeeper.instructions;
            this.zookeeper.instructions = InstructionStub;

            spyOn(Zookeeper.prototype, 'reportToBoss').and.callFake(function () {
            });

            spyOn(Zookeeper.prototype, 'followInstruction').and.callFake(function () {
            });

            spyOn(Zoo, 'post').and.callFake(function () {
            });

            this.zookeeper.followInstructions();
        });

        afterEach(function () {
            this.zookeeper.instructions = this._instructions;
        });

        it('follows instructions it can follow', function () {
            expect(this.zookeeper.followInstruction).toHaveBeenCalledWith({
                Elephant: {
                    Feed: 'Cheese'
                }
            });
            expect(this.zookeeper.followInstruction).toHaveBeenCalledWith({
                Tiger: {
                    Feed: 'Bacon'
                }
            });

        });

        it('doesn\'t follow instructions it can\'t follow', function () {
            expect(this.zookeeper.followInstruction).not.toHaveBeenCalledWith({
                Snake: {
                    Feed: 'Mouse'
                }
            });
        });

        it('reports back to the boss', function () {
            expect(this.zookeeper.reportToBoss).toHaveBeenCalledWith({
                Done: {
                    Elephant: 1,
                    Tiger: 1
                },
                Violations: {
                    Snake: 'Not trained'
                }
            });
        });

    });

});